#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

unsigned int n;

string Korale(unsigned int n) {
	string boi = "";
	
	if(n == 1) {
		boi += '1';
		return boi;
	}
	else if(n % 2 == 0) {
		boi += Korale(n/2);
		boi += '0';
		return boi;
	}
	else if(n % 2 == 1) {
		boi += Korale((n-1)/2);
		boi += '1';
		return boi;
	}
}

string KoraleBis(unsigned int n) {
	string boi = "";
	
	while(n >= 1) {
		if(n % 2 == 1) {
			 boi = '1' + boi;
			 n = (n - 1) / 2;
		}
		else if(n % 2 == 0) {
			 boi = '0' + boi;
			 n = n / 2;
		}
	}
	
	return boi;
}

int main() {
	cout << "n = ";			cin >> n;
	
	cout << Korale(n) << endl;
	cout << KoraleBis(n) << endl;
	
	return 0;
}
