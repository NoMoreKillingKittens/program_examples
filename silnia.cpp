#include <iostream>
using namespace std;

unsigned long long x, out;

int main() {
	cout << "x = ";			cin >> x;
	if((x == 0) || (x == 1)) {out = 1;}
	else {out = 1; for(int i = 2; i <= x; ++i) out *= i;}
	cout << x << "! = " << out << endl;
	return 0;
}